#!/bin/sh

## Screenshot: a helper for screenshots within sway
##             Copied and modified from grimshot
## Requirements:
##  - `grim`: screenshot utility for wayland
##  - `slurp`: to select an area
##  - `swaymsg`: to read properties of current window
##  - `wl-copy`: clipboard utility
##  - `jq`: json utility to parse swaymsg output
##  - `notify-send`: to show notifications

__usage="
Usage: $(basename $0) [OPTIONS] [COMMAND] [FILE|-]

Commands:
  copy:    Copy the screenshot data into the clipboard.
  save:    Save the screenshot to a regular file or '-' to pipe to STDOUT.
  help:    Show this message and exit.

FIlE:      Output file to use when saving, can use 'date' formatting.
           Defaults to '~/Pictures/screenshot_%F_%T.png'

Options:
  -h,--help:              Show this message and exit.
  -n,--notify:            Show a notification using notify-send.
  -f,--scale-factor:      Output image scale factor.
  -c,--cursor:            Include cursors in the screenshot.
  -d,--dimensions:        Display the dimensions of area selection.
  -w,--border-width:      Area selection border width.
  -g,--color-background:  Area selection background color.
  -b,--color-border:      Area selection border color.
  -s,--color-selection:   Area selection selection color.
"

NOTIFY=no
GRIM_ARGS=''
SLURP_ARGS=''
POSITIONAL=()

while [[ $# -gt 0 ]]; do
	case "$1" in
		'--')
			break 2
			;;
		'-h' | '--help' | 'help')
			echo "$__usage"
			exit 1
			;;
		'-n' | '--notify')
			NOTIFY=yes
			;;
		'-f' | '--scale-factor')
			GRIM_ARGS="$GRIM_ARGS -s $2"
			shift
			;;
		'-c' | '--cursor')
			GRIM_ARGS="$GRIM_ARGS -c"
			;;
		'-d' | '--dimensions')
			SLURP_ARGS="$SLURP_ARGS -d"
			;;
		'-w' | '--border-width')
			SLURP_ARGS="$SLURP_ARGS -w $2"
			shift
			;;
		'-g' | '--color-background')
			SLURP_ARGS="$SLURP_ARGS -b $2 -B $2"
			shift
			;;
		'-b' | '--color-border')
			SLURP_ARGS="$SLURP_ARGS -c $2"
			shift
			;;
		'-s' | '--color-selection')
			SLURP_ARGS="$SLURP_ARGS -s $2"
			shift
			;;
		*)
			POSITIONAL+=("$1")
			;;
	esac
	shift
done

notify() {
	notify-send -t 3000 -a screenshot "$@"
}
notifyOk() {
	[ "$NOTIFY" = "no" ] && return

	TITLE=${2:-"Screenshot"}
	MESSAGE=${1:-"OK"}
	notify "$TITLE" "$MESSAGE"
}
notifyError() {
	if [ $NOTIFY = "no" ]; then
		echo $1
		return
	fi

	TITLE=${2:-"Screenshot"}
	MESSAGE=${1:-"Error taking screenshot"}
	notify -u critical "$TITLE" "$MESSAGE"
}
die() {
	notifyError "Error: $1"
	exit 2
}
takeScreenshot() {
	grim $GRIM_ARGS -g "$GEOM" "$@" || die "Unable to invoke grim"
}

ACTION=${POSITIONAL[0]:-save}
FILE=$(date +"${POSITIONAL[1]:-"$HOME/Pictures/screenshot_%F_%T.png"}")
GEOM=$(swaymsg -t get_tree | jq -r '.. | select(.pid? and .visible?) | .rect | "\(.x),\(.y) \(.width)x\(.height)"' | slurp -o $SLURP_ARGS)

# Check if user exited slurp without selecting the area
if [ -z "$GEOM" ]; then
	exit
fi

if [ "$ACTION" = 'copy' ]; then
	takeScreenshot - | wl-copy --type image/png || die "Clipboard error"
	notifyOk "Copied to clipboard"
elif [ "$ACTION" = 'save' ]; then
	takeScreenshot "$FILE"
	notifyOk "$(basename "$FILE")"
	echo $(realpath $FILE)
else
	echo "$__usage"
	exit 1
fi

